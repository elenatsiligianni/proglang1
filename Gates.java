import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**Class that represents a gate whose members are the
 * keys needed to open it, and the keys and coins behind the gate
 */
class Gate{
    private HashMap<Integer, Integer> inK;
    private HashMap<Integer, Integer> outK;
    private int coins;

    Gate(HashMap<Integer, Integer> inK, HashMap<Integer, Integer> outK, int coins){
        this.inK = inK;
        this.outK = outK;
        this.coins = coins;
    }

    public HashMap<Integer, Integer> getInKeys() {
        return inK;
    }

    public HashMap<Integer, Integer> getOutKeys() {
        return outK;
    }

    public int getCoins() {
        return coins;
    }
}

/**
 * Class that handles the search of the maximum profit
 */
class GatesManager{

    /**
     * Uses (removes) the keys to open the gate
     * @param keys
     * @param gate
     */
    void useKeys(HashMap<Integer, Integer> keys, Gate gate){
        for (Entry<Integer, Integer> kvp: gate.getInKeys().entrySet()){
            keys.put(kvp.getKey(), keys.get(kvp.getKey()) - kvp.getValue());
        }
    }

    /**
     * Collects the keys behind the gate
     * @param keys
     * @param gate
     */
    void collectKeys(HashMap<Integer, Integer> keys, Gate gate){
        for(Entry<Integer, Integer> kvp: gate.getOutKeys().entrySet()){
            if(keys.containsKey(kvp.getKey()))
                keys.put(kvp.getKey(), keys.get(kvp.getKey()) + kvp.getValue());
            else
                keys.put(kvp.getKey(), kvp.getValue());
        }
    }

    /**
     * Checks if a gate can be opened with the current keys
     * @param keys
     * @param gate
     * @return
     */
    boolean canOpen(HashMap<Integer, Integer> keys, Gate gate){
        for (Entry<Integer, Integer> kvp: gate.getInKeys().entrySet()){
            if(!keys.containsKey(kvp.getKey()))
                return false;
            if(keys.get(kvp.getKey()) < kvp.getValue())
                return false;
        }
        return true;
    }

    /**
     * Checks if the gate can be safely opened. A gate can be safely opened when it returns the keys
     * used to open it. This is always safe to do because always maximizes the earnings.
     * @param gate
     * @return
     */
    boolean safeGate(Gate gate){
        for (Entry<Integer, Integer> kvp: gate.getInKeys().entrySet()){
            if(!gate.getOutKeys().containsKey(kvp.getKey()))
                return false;
            if(gate.getOutKeys().get(kvp.getKey()) < kvp.getValue())
                return false;
        }
        return true;
    }

    /**
     * Open all the safe gates, returning the coins found behind them.
     * @param keys
     * @param gates
     * @return
     */
    int safeOpen(HashMap<Integer, Integer> keys, ArrayList<Gate> gates){
        int score = 0;
        ArrayList<Gate> toRemove = new ArrayList<Gate>();

        for (Gate gate: gates){
            if (safeGate(gate) && canOpen(keys, gate)){
                useKeys(keys, gate);
                collectKeys(keys, gate);
                toRemove.add(gate);
                score += gate.getCoins();
            }
        }

        gates.removeAll(toRemove);
        return  score;
    }

    /**
     * This is the main recursive method which explores the search space opening first the safe gates.
     * @param keys
     * @param gates
     * @return
     */
    int openGates(HashMap<Integer, Integer> keys, ArrayList<Gate> gates){

        int profit = safeOpen(keys, gates);
        int tempProfit = 0;

        for(Gate gate: gates){
            int gateProfit = 0;

            if(canOpen(keys, gate)){

                HashMap<Integer, Integer> keysClone = new HashMap<Integer, Integer>();

                for(Entry<Integer, Integer> kvp: keys.entrySet())
                    keysClone.put(kvp.getKey(), kvp.getValue());

                ArrayList<Gate> gatesClone = new ArrayList<Gate>();

                for(Gate g: gates)
                    gatesClone.add(g);

                gatesClone.remove(gate);
                useKeys(keysClone, gate);
                collectKeys(keysClone, gate);
                gateProfit += gate.getCoins();
                gateProfit += openGates(keysClone, gatesClone);
            }

            if(gateProfit > tempProfit)
                tempProfit = gateProfit;
        }

        return profit + tempProfit;
    }
}

public class Gates {

    public static void main(String[] args){

        String path = args[0];

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));

            reader.readLine();

            HashMap<Integer, Integer> keys = new HashMap<Integer, Integer>();

            for (String s: reader.readLine().split(" ")){
                int key = Integer.valueOf(s);
                if(keys.containsKey(key))
                    keys.put(key, keys.get(key) + 1);
                else
                    keys.put(key, 1);
            }

            String line;
            ArrayList<Gate> gates = new ArrayList<Gate>();

            while ((line = reader.readLine()) != null) {
                String[] gate = line.split(" ");
                int inKN = Integer.valueOf(gate[0]);
                int coins = Integer.valueOf(gate[2]);

                HashMap<Integer, Integer> inK = new HashMap<Integer, Integer>();
                HashMap<Integer, Integer> outK = new HashMap<Integer, Integer>();

                for (int i = 3; i < 3 + inKN; i++){
                    int key = Integer.valueOf(gate[i]);

                    if(inK.containsKey(key))
                        inK.put(key, inK.get(key) + 1);
                    else
                        inK.put(key, 1);
                }

                for (int i = 3 + inKN; i < gate.length; i++){
                    int key = Integer.valueOf(gate[i]);

                    if(outK.containsKey(key))
                        outK.put(key, outK.get(key) + 1);
                    else
                        outK.put(key, 1);
                }

                gates.add(new Gate(inK, outK, coins));
            }

            GatesManager gm = new GatesManager();
            System.out.println(gm.openGates(keys, gates));

        } catch (IOException x) {
            System.err.println(x);
        }
    }
}
