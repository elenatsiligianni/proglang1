/*
 * An auxiliary predicate that reads a line and returns the list of
 * integers that the line contains.
 */
read_line(Stream, List) :-
    read_line_to_codes(Stream, Line),
    atom_codes(A, Line),
    atomic_list_concat(As, ' ', A),
    maplist(atom_number, As, List).

countries(File, R, E):-  open(File, read, Str), read_line(Str, [_]), read_line(Str, Ws), close(Str), msort(Ws,L1),reverse(L1,L2),eliminate(L2,E,R).

eliminate(L,0,R):-balanced(L),length(L,R),!.
eliminate([H|T],E,R):-absorb([H|T],R1),eliminate(T,E1,R2),R3 is max(R1,R2),
	       ((R1=:=R2,R=R1,E is 0,!);(R3=:=R1,E=0,R=R1,!);(R3=:=R2,E is E1+1,R is R2)).

balanced([_|[]]):-!.
balanced([]):-!.
balanced([H|T]):-last(T,H1),H2 is 2*H1,H2>H.

absorb([],0):-!.
absorb([_|[]],1):-!.
absorb([H|T],R):-count_remaining(H,T,R1),R is R1+1.

count_remaining(_,[],0):-!.
count_remaining(H,[H1|_],0):-H2 is H1*2,H2<H,!.
count_remaining(H,[_|T1],R):-count_remaining(H,T1,R1),R is R1+1.


