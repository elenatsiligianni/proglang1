read_and_return(File, Ks, Gs) :-
    open(File, read, Stream),
    read_line(Stream, [K, N]),
    ( K > 0 -> read_line(Stream, Ks) ; K =:= 0 -> Ks = [] ),
    read_gates(Stream, N, Gs),
    close(Stream).

read_gates(Stream, N, Gates) :-
    ( N > 0 ->
	Gates = [G|Gs],
        read_line(Stream, [K1, K2, Money | KeyList]),
	length(InKeys, K1),
	append(InKeys, OutKeys, KeyList),
	length(OutKeys, K2),
	G = [InKeys, OutKeys, Money],
        N1 is N - 1,
        read_gates(Stream, N1, Gs)
    ; N =:= 0 ->
	Gates = []
    ).

read_line(Stream, List) :-
    read_line_to_codes(Stream, Line),
    atom_codes(A, Line),
    atomic_list_concat(As, ' ', A),
    maplist(atom_number, As, List).

%simulates the opening of a gate
open_gate([K,Ko,Mo],Gates,Keys,M,GatesP,KeysR,MP):- delete_keys(Keys,K,KeysR1),
		                                    append(KeysR1,Ko,KeysR),
					            select([K,Ko,Mo],Gates,GatesP),
						    MP is M+Mo.

%deletes keys of the list
delete_keys(Keys,[],Keys):-!.
delete_keys(Keys,[H|T],KeysR):-select(H,Keys,L1),
			       delete_keys(L1,T,KeysR).

%finds gates can be opened with the current list of keys
possible_open([],_,[]):-!.
possible_open([H|T],Keys,[H|R]):-H=[K,_,_],can_open(K,Keys),possible_open(T,Keys,R),!.
possible_open([_|T],Keys,R):-possible_open(T,Keys,R).

safe_open([],_,[]):-!.
safe_open([H|T],Keys,[H|R]):-H=[K,Ko,_],can_open(K,Ko),safe_open(T,Keys,R),!.
safe_open([_|T],Keys,R):-safe_open(T,Keys,R).

%checks if a gate can be opened with the current list of keys
can_open([],_):-!.
can_open([H|T],Keys):-member(H,Keys),select(H,Keys,Keys1),can_open(T,Keys1).

gates(File, MaxMoney):-read_and_return(File, K, G), gates1(G, K, MaxMoney).

gates2(Gates,Keys,Mo):-possible_open(Gates,Keys,L),safe_open(L,Keys,R),
		      open_all(R,Gates,Keys,0,GatesP,KeysP,Mc),
		      possible_open(GatesP,KeysP,T),gates_aux(T,GatesP,KeysP,Mc,Mo).

gates1(Gates,Keys,Mo):-possible_open(Gates,Keys,Possible),
	               open_all_save(Gates,Keys,Possible,GatesP,KeysP,Mo1,R),
		       gates_aux(R,GatesP,KeysP,Mo1,Mo).

open_all_save(Gates,Keys,Possible,Gates,Keys,0,Possible):-safe_open(Possible,Keys,H),length(H,0),!.

open_all_save(Gates,Keys,Possible,RGates,RKeys,Mo,Rem):-safe_open(Possible,Keys,O),
						        open_all(O,Gates,Keys,0,GatesP,KeysP,Mo1),
						        possible_open(GatesP,KeysP,R),
					                open_all_save(GatesP,KeysP,R,RGates,RKeys,Mo2,Rem),Mo is Mo1+Mo2, !.
open_all([],Gates,Keys,M,Gates,Keys,M):-!.
open_all([H|T],Gates,Keys,M,GatesP,KeysP,Mc):-open_gate(H,Gates,Keys,M,GatesPP,KeysPP,McP),
					      open_all(T,GatesPP,KeysPP,McP,GatesP,KeysP,Mc).

gates_aux([],_,_,Mo,Mo):-!.

gates_aux([H|T],Gates,Keys,Mc,Mo):-  open_gate(H,Gates,Keys,Mc,GatesP,KeysR,MP),
				     gates1(GatesP,KeysR,Mo1),Mo2 is Mo1+MP,
				     gates_aux(T,Gates,Keys,Mc,Mo3),
				     Mo is max(Mo3,Mo2),!.
