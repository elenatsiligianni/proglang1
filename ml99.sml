(*
* Tuesday 14 November 2012 
* Eleni Tsiligianni
* 99 problems of Prolog, solved in ML
* https://sites.google.com/site/prologsite/prolog-problems
*)


(* Lists *)

(* Find the last element of a list. *)
fun last [ H] = H
|	last ( _ :: T) = last T
;

(* Find the last but one element of a list. *)
fun beforeLast [ H1, H2] = H1
|	beforeLast ( _ :: T) = beforeLast T
;

(* Find the K'th element of a list. *)
fun findKth ( H :: _) 1 = H
|	findKth ( _ :: T) K =
		let
		    	val NewK = K - 1
		in
    			findKth T NewK
		end
;

(* Find the number of elements of a list. *)
fun my_length [] = 0
|	my_length ( _ :: T) = 1 + my_length T
;

(* Reverse a list. *)
fun reverse ( [], z) = z
|	reverse ( x :: xs, l) = reverse ( xs, x :: l)
;

(* Compare two lists. It is not applicable to lists of real numbers*)

fun compare l1 l2 = l1 = l2
;
(* Find out whether a list is palindrome. It is not applicable to lists of real numbers*)
fun palindrome [] = true
| palindrome l  = 
	let
		val revl = reverse ( l, [])
	in
		revl = l
	end
;
(* Flatten a nested list structure. The nested list is a list of lists exactly. *)
fun flatten [] = []
|	flatten ( x :: xs) = x @ flatten xs
;

(* Eliminate consecutive duplicates of list elements. *)
fun eliminate [] = []
|	eliminate ( x :: []) = [ x]
|	eliminate ( y :: z :: rest) = if y = z then eliminate ( y :: rest) 
						else [ y] @ eliminate  ( z :: rest)
;

(* Pack consecutive duplicates of the head of a list into a list. *)
(* ( the desired list, the remainder of the initial list) *)
fun packOne [] = ( [], [])
|	packOne [ x] = ( [ x], [])
|	packOne ( y :: z :: tail) = if y = z then
						let
							val ( yrest, rest) = packOne ( y :: tail)
						in
							( [ y] @ yrest, rest)
						end
					     else ( [ y], z :: tail)
;
(* Pack consecutive duplicates of list elements into sublists. Assume that input has at least one element *)
fun packAll [] = []
|	packAll l =
		let
			val ( xl, rest) = packOne l
		in
			xl :: packAll rest
		end
;

(* Run-length encoding of a list. *)
(* each pair of the returned list is a tuple at the form ( number of consecutive occurrences of an element, element) *)
fun encode [] = []
|	encode l =
		let
			val ( x, xs) = packOne l
			val xl = length x
			val xhd = hd x
		in
			( xl, xhd) :: encode xs	
		end
;

(* Takes an encoded pair and turns it to a list. *)
fun decodeOne ( 0, e) = [ ]
|	decodeOne ( t, e) =
			let
				val newT = t - 1
			in
				e :: decodeOne ( newT, e)
			end
;

(* Decode a run-length encoded list. *)
fun decode [] = []
|	decode ( x :: xs) =
			let
				val xl = decodeOne x
			in
				xl @ decode xs
			end
;

(* Find the length of a sublist where all of its elements are identical. *)
fun subLength [] = ( 0, [])
|	subLength [ x] = ( 1, [])
|	subLength ( y :: z :: tail) = if y = z then 
							let
								val ( yl, rest) = subLength ( y :: tail)
							in
								( 1 + yl, rest)
							end
						   else ( 1, z :: tail)
;

(* Run-length encoding of a list (direct solution). *)
fun direct [] = []
|	direct [ x] = [ ( 1, x)]
|	direct ( y :: z :: tail) = 
				let
					val ( yl, rest) = subLength ( y :: z :: tail)
				in
					( yl, y) :: direct rest
				end
;

(* Duplicate the elements of a list. *)
fun dupli [] = []
|	dupli [ x] = [ x, x]
|	dupli ( x :: xs) = x :: x :: dupli xs
;

(* Multiplicate the elements of a list a given number of times. *)
fun mulN n x = n * x

fun multiplicate [] _ = []
|	multiplicate list n = map ( mulN n) list
;

(* Count n consecutives elements of a list. Assume n >= 0. *)
(* returns a tuple at the form ( the sublist with the first n elements, the remainder list) *)
fun countN [] _ = ( [], [])
|	countN list 0 = ( [], list)
|	countN ( h :: t) 1 = ( [ h], t)
|	countN ( x :: xs) n = if n > length ( x :: xs) then ( ( x :: xs), [])
					else 
						let
							val newN = n - 1
							val ( xr, rest) = countN xs newN
						in
							( x :: xr, rest)
						end
;

(* Drop every N'th element from a list. *)
fun drop [] _ = []
|	drop l 0 = l
|	drop _ 1 = []
|	drop list n = if n > length list
			then list
			else
				let
					val newN = n - 1			
						val ( xr, x :: xs) = countN list newN
					in
						if n <= length xs
							then
								xr @ drop xs n
							else
								xr @ xs
					end
;

(* ARITHMETIC *)

(* Construct an interval of numbers from l to r, including l and r. *)
fun interval l r = if not( l > r)
			then
					l :: interval ( l + 1.0) r
			else
				[ ]
;

(* Construct an interval of odd numbers between l, r, including l and r if they are odd. *)
fun intervalOdd l r =
		 if not ( l > r)
			then
				let
					val newL = l + 1
				in
					if l mod 2 = 0
						then
							intervalOdd newL r
						else
							l :: intervalOdd newL r
				end
			else
				[ ]
;

(* Construct an interval of even real numbers between l, r, including l and r if they are even. *)
fun intervalEven l r =
		 if not ( l > r)
			then
				if floor( l) mod 2 <> 0
					then
						intervalEven ( l + 1.0) r
					else
						l :: intervalEven ( l + 1.0) r
			else
				[ ]
;

(* Returns the modulo of a number n divided by d. *)
fun divN n d = n mod d
;

(* Checks if an element is a member of a list. *)
fun member x [] = false
|	member x ( y :: ys) = if x = y
				then
					true
				else
					member x ys
;
 
(* Checks if a list of modulos includes 0. *)
fun testPrime l = member 0 l
;

(* Determine whether a given number is prime. The given number is integer but is considered as real to accelerate program execution. *)
fun isPrime p = let
			val sqr = ceil( sqrt( p))
			val intP = floor( p)
			val i = 2 :: intervalOdd 3 sqr
			val modl = map ( divN intP) i
		in
			case intP of
			1	=> true
			| 2	=> true
			| intP	=> not( testPrime modl)
		end			
;

(* Determine the prime factors of a given positive integer. The integer is given as a real number to accelerate program execution. *)
fun primeFactors l r = if isPrime l
			then
				if ( floor( r) mod floor( l)) = 0
					then
						floor( l) :: primeFactors l ( r / l)
					else
						primeFactors ( l + 1.0) r
			else
				if l < r
					then
						if isPrime r
							then
								[ floor( r)]
							else
								primeFactors ( l + 1.0) r
					else
						[]
;

(* Return the prime factors of a given positive integer in run - length encoding *)
fun factorsEncoded p = direct ( primeFactors 2.0 p)
;

(* Construct an interval that includes only prime numbers between l, r. *)
fun intPrimes l r = if l > r
			then
				[]
			else
				if isPrime l
					then
						floor( l) :: intPrimes ( l + 1.0) r
					else
						intPrimes ( l + 1.0) r
;

(* Add a number n to a number x *)
fun addN n x = n + x
;

(* Check Goldbach's condition for a whole list. *)
fun gold [] _ = []
|	gold ( x :: xs) e =  	let
					fun goldCondition n l = map ( addN n) l
				in	
					if ( member ( floor( e))  ( goldCondition x xs))
						then
							[ x, floor( e) - x]
						else
							gold xs e
				end
;

(* Goldbach's conjecture. *)
fun goldbach e =	let
				val ( x :: xs) = intPrimes 1.0 e
			in
				gold ( x :: xs) e
			end
;

(* Find Goldbach's conjecture for a list of numbers, even only *)
fun goldList x y =	let
				fun goldenList [] = []
				|	goldenList [ z] = ( floor( z), goldbach z) :: []
				|	goldenList ( b :: bs) =  ( floor( b), goldbach b) :: goldenList bs
				val ( x :: xs) = intervalEven x y
			in
				goldenList ( x :: xs)
			end
;

(* Print a number's Goldbach's conjecture pair where both of its elements exceed a given threshold *)
fun goldThreshold [] _ _ = []
|	goldThreshold ( x :: xs) e th =
		if x < floor( th)
			then
				goldThreshold xs e th
			else
				let
					fun goldCondition n l = map ( addN n) l
				in	
					if not( ( member ( floor( e))  ( goldCondition x xs))) orelse ( floor( e) - x < floor( th))
						then
							goldThreshold xs e th
						else
							[ x, floor( e) - x]
						end
;

fun goldbachThreshold e th = 	let
					val xl = intPrimes th e
				in
					goldThreshold xl e th

				end
;

fun goldenListThreshold [] _ = []
|	goldenListThreshold [ z] th =	let
						val zl = goldbachThreshold z th
					in
						if null zl
							then
								[]
							else
								( floor( z), zl) :: []
					end
|	goldenListThreshold ( b :: bs) th =	let
							val bl = goldbachThreshold b th
						in	
							if null bl
								then
									goldenListThreshold bs th
								else
									( floor( b), bl) :: goldenListThreshold bs th
						end
;
fun goldListThreshold x y th = 	let
					val ( z :: zs) = intervalEven x y
				in
					goldenListThreshold ( z :: zs) th
				end
;

(* Determine the greatest common divisor of two positive integer numbers. *)
fun gcd 0 y = y
|	gcd x 0 = x
|	gcd z w = if z = w 
			then
				z
			else
				if z > w 
					then
						gcd ( z mod w) w
					else
						gcd z ( w mod z)
;

(* Determine whether two positive integer numbers are coprime. *)
fun coprime x y = 	let
				val g = gcd x y
			in
				if g = 1
					then
						true
					else
						false
			end
;

(* Returns the amount of coprime numbers that are less than a given number m *)
fun totient x m = if ( floor x) > m
			then
				0
			else
				if isPrime x
					then
						1 + totient ( x + 1.0) m
					else
						if( coprime ( floor x) m)
							then
								1 + totient ( x + 1.0) m
							else
								totient ( x + 1.0) m
;

(* Calculate Euler's totient function phi( f). *)
fun eulerTotient m =	if m < 2.0
				then
					0
				else
					if isPrime m
						then
							floor( m) - 1
						else
							let
								val i = interval  1.0 m
								val mf = floor( m)
							in
								totient 1.0  mf
							end
;

(* Compute x^y for integers. *)
fun intPow 0 y = 0
|	intPow x 0 = 1
|	intPow z w = z * intPow z ( w - 1)
;

(* Computes the euler totient of a number based on its prime factors. *)
fun totientPrimes [] = 1
|	totientPrimes ( ( t, e) :: xs) = ( e - 1) * ( intPow e ( t - 1)) * totientPrimes xs
;

(* Calculate Euler's totient function phi( f) by prime factors. *)
fun eulerPrimes m =	let
				val ip = factorsEncoded m
			in
				totientPrimes ip
			end
;

(* Split a list according to ratio r. *)
fun split [] cl _ = ( cl, [])
|	split ( x :: xs) sl 1 = ( sl @ [ x], xs)
|	split ( y :: ys) ul l = split ys ( ul @ [ y]) ( l - 1)
;

(* Add a character to the head of as string. *)
fun stringHead c str = c ^ str
;

(* Construct the halves of gray code and concatenate them. *)
fun grayN [] r = map ( stringHead "1") r
|	grayN l [] = map ( stringHead "0") l
|	grayN h t = ( map ( stringHead "0") h) @ ( map ( stringHead "1") t)
;

(* Gray code. *)
fun gray 1 = [ "0", "1"]
|	gray n =	let
				val revn = reverse ( gray ( n - 1), [])
				val xl = ( gray ( n - 1)) @ revn
				val len = ( length xl) div 2
				val ( l, r) = split xl [] len
			in
				grayN l r				
			end
;
