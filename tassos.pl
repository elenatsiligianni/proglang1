provata(File, MinCost):-read_and_return(File, G), compact_list(G, Dir), tassos(Dir, [], MinCost).

%delete_all(L1,L2,L3) removes al items in L2 from L, and stores the result in L3.
delete_all(Dir,[],Dir):-!.
delete_all(Dir,[H|T],DirP):-select(H,Dir,L),delete_all(L,T,DirP).

%rest(L1,L2,L3) finds the larger suffix of L1 that doesn't contain
%items from L2 and stores the result in L3
rest(Stack,[],Stack):-!.
rest([H|T],S,Sp):-member(X,S),member(X,T),not(member(H,S)),rest(T,S,Sp),!.
rest([H|T],S,Sp):-member(H,S),select(H,S,S1),rest(T,S1,Sp).

%finds all possible sheepfolds to go
possible_targets(Dir,Stack,R):-findall(X,(member([X,_,_],Dir)),L), findall(Y,(member([_,Y,_],Stack)),L1),
			       append(L,L1,L2), list_to_set(L2,R).

%compact the list in the form [X,Y,Count], where Count is the number of
%ocurrences of [X,Y] in the list.
compact_list([],[]):-!.
compact_list([[X1,X2]|T],[[X1,X2,C1]|R]):-compact_list(T,R1),member([X1,X2,C],R1),select([X1,X2,C],R1,R), C1 is C+1,!.
compact_list([[X1,X2]|T],[[X1,X2,1]|R]):-compact_list(T,R).

%sum the cost of make load(unload) of all the items from a list.
sum_cost([],0):-!.
sum_cost([[_,_,C]|T],R):-sum_cost(T,R1), R is R1+C.

%Dir is the list of all sheep to move
tassos(Dir,Stack,Cost):-possible_targets(Dir,Stack,R), tassos_aux_mult(R,Dir,Stack,Cost).

%returns all possible states (State of the Stack, Cost an state of Dir)
%after go to an sheepfold
go_to_mult(H,Dir,Stack,Results):- sum_cost(Stack,C1),findall(X,(member(X,Stack),X=[_,H,_]),S),
				  delete_all(Stack,S,StackP),rest(Stack,S,StackPP),
				  findall(Y,(member(Y,Dir),Y=[H,_,_]),L1),
				  delete_all(Dir,L1,DirP),
				  append(StackP,L1,L),sum_cost(L,C2),
				  findall(V,(permutation(Z,L),common_suffix(StackPP,Z,Cs),
				  sum_cost(Cs,C3), C is C1+C2-2*C3,V=[C,Z,DirP]),Results).

go_to_mult1(H,Dir,Stack,Results):- findall(X,(member(X,Stack),X=[_,H,_]),S),
				  delete_all(Stack,S,StackP),rest(Stack,S,StackPP),
				  sum_cost(Stack,Cs),sum_cost(StackPP,Cpp),C1 is Cs-Cpp,
				  findall(Y,(member(Y,Dir),Y=[H,_,_]),L1),
				  delete_all(Dir,L1,DirP),
				  delete_all(StackP,StackPP,S1),
				  append(S1,L1,L),sum_cost(L,C2),C is C1+C2,
				  findall(V,(permutation(Z,L),append(Z,StackPP,Y1),
				  V=[C,Y1,DirP]),Results).
%computes the common suffix of two list
common_suffix(L1,L2,L):-reverse(L1,L11),reverse(L2,L21),common_prefix(L11,L21,L).
%computes the common prefix of two list
common_prefix([],_,[]):-!.
common_prefix(_,[],[]):-!.
common_prefix([H|T1],[H|T2],[H|T]):-common_prefix(T1,T2,T),!.
common_prefix(_,_,[]).
% makes the full search of the solution
tassos_aux_mult([],_,_,0):-!.
tassos_aux_mult([H],Dir,Stack,Cost):-go_to_mult(H,Dir,Stack,Results),
				     min_cost(Results,CostP),Cost is CostP+3.
tassos_aux_mult([H,H1|T],Dir,Stack,Cost):-go_to_mult(H,Dir,Stack,Results),
	                             min_cost(Results,CostP),
	                             CostP1 is CostP+3,
				     tassos_aux_mult([H1|T],Dir,Stack,CostP2),
				     Cost is min(CostP1,CostP2).

min_cost([[Cost1,Stack1,Dir1]],Cost):-tassos(Dir1,Stack1,Cost2), Cost is Cost1+Cost2,!.
min_cost([[Cost1,Stack1,Dir1]|T],Cost):-tassos(Dir1,Stack1,Cost2), CostP is Cost1+Cost2,
					min_cost(T,CostP1),Cost is min(CostP,CostP1).


%To read the input.....
read_and_return(File, Gs) :-
    open(File, read, Stream),
    read_line(Stream, [_, N]),
    read_gates(Stream, N, Gs),
    close(Stream).

read_gates(Stream, N, Gates) :-
    ( N > 0 ->
	Gates = [G|Gs],
        read_line(Stream, [K1, K2]),
	G = [K1, K2],
        N1 is N - 1,
        read_gates(Stream, N1, Gs)
    ; N =:= 0 ->
	Gates = []
    ).

read_line(Stream, List) :-
    read_line_to_codes(Stream, Line),
    atom_codes(A, Line),
    atomic_list_concat(As, ' ', A),
    maplist(atom_number, As, List).


